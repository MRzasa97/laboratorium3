tree grammar TExpr1;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;
          
print   : ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());};



expr returns [Integer out]
        : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr){$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr){$out = div($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr){$out = setVar($e1.text, $e2.out);}
        | ^(BITAND e1=expr e2=expr){$out = bitAND($e1.out, $e2.out);}
        | ^(BITOR e1=expr e2=expr){$out = bitOR($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                        {$out = getVar($ID.text);}
        ;
        catch [RuntimeException e] {System.out.println(e.getMessage());}
        
declaration:      ^(VAR i1=ID) {createVar($i1.text);};